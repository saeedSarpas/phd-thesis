#! /bin/python3

import glob
import os

pngs = list(glob.glob('*.png'))
# pngs += ['./sb_evolution/' + p for p in glob.glob('./SB_evolution/*.png')]
print(len(pngs))

l = int(len(pngs) / 5)
print(l)
for i in range(l):
    init = i * l
    end = i * (l+1) if i != l-1 else len(pngs) - 1
    cmd = 'mutt -s pngs ' + \
        ' '.join(['-a ' + p for p in pngs[init:end]]) + \
        ' -- s.sarpas@gmail.com'

    print(cmd)
    os.system(cmd)
