{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Rhyme"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## TOC\n",
    "\n",
    "* [Abstract](#abstract)\n",
    "* [Introduction](#introduction)\n",
    "* [Numerical Methods](#numerical-methods)\n",
    "    * [Tile-Based Adaptive Mesh Refinement](#num-amr)\n",
    "    * [Hydrodynamical Solver](#num-hyd)\n",
    "        * [Conservation laws](#num-hyd-con)\n",
    "        * [Slope Limiters](#num-hyd-slo)\n",
    "            * [van Leer 1977](#num-hyd-slo-lee)\n",
    "    * [Radiative Transfer Solver](#num-rts)\n",
    "* [Tests](#tests)\n",
    "    * [Hydrodynamics](#tes-hyd)\n",
    "        * [Sod's Tube](#tes-hyd-sod)\n",
    "        * [Two shocks](#tes-hyd-two)\n",
    "        * [Left and right blast waves](#tes-hyd-lrb)\n",
    "        * [Mach reflection](#tes-hyd-mac)\n",
    "        * [Kelvin-Helmholtz Instability](#tes-hyd-khi)\n",
    "    * [Radiative transfer](#tes-rad)\n",
    "    * [Radiation Hydrodynamics](#tes-rhd)\n",
    "        * [Evaporation of a clump (Iliev test #7)](#tes-rhd-il7)\n",
    "* [Conclusion](#conclusion)\n",
    "* [Outlook](#outlook)\n",
    "* [References](#references)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Abstract  <a class=\"anchor\" id=\"abstract\"></a>\n",
    "\n",
    "We are presenting a new Radiation Hydrodynamics (RHD) code, called Rhyme.\n",
    "We use this code to study the properties of the cold component of the CGM of bright and distant quasars through a set of idealized simulations.\n",
    "A Monte-Carlo Ray tracing (RT) scheme is being adopted from the RADAMESH code ([Cantalupo & Porciani 2011](#ref-cantalupo-2011)) for solving the radiative transfer equation.\n",
    "The energy which is transported to each cell is then being used as a source term of the energy conservation equation where we are using an unsplit 2nd-order MUSCL-Hancock scheme with a choice between an exact or a Neural Network based 1D Riemann solver to solve Euler equations with high accuracy.\n",
    "The code can be used in 1D, 2D and 3D simulations with or without a tiling adaptive mesh refinement with arbitrary number of refinement levels.\n",
    "Even though the code has been develop based on test driven development best practices and each component has been tested separately, results of a suit of usual hydrodynamics, radiative transfer and RHD tests is being presented to show the accuracy of the code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction  <a class=\"anchor\" id=\"introduction\"></a>\n",
    "\n",
    "- Imprtance of RHD simulations (radiation is an active agent)\n",
    "- The importance of the accuracy of temperature and ionization fractions\n",
    "- RHD refers to a large set of numerical simulations\n",
    "- Break RHD into transfer and Euler equation\n",
    "- RT\n",
    "    - Solving the transfer equation\n",
    "    - Moment-based methods (and their pros and cons)\n",
    "- Hydrodynamics\n",
    "    - Solving Euler equations\n",
    "    - Lagrangian, Eulerian and intermediate schems\n",
    "        - Lagrangian:\n",
    "            Using a moving mesh to follow the geometry of the flow helps to keep the solution of the Euler equations Galilean invariant however it sufferes from numerous mesh updates\n",
    "        - Eulerian:\n",
    "            Based ona static uniform Cartesian grid\n",
    "            Suffers from the limited dynamics range\n",
    "        - Intermediate:\n",
    "            SPH is a good particle-based example of intermediate schemes where the particles follow the flow of the fluid. Then using a \"smoothing kernel\", the average properties of the finite number of neighboring particles are being used to define the resolution element where the Euler equations would be solved based on. Due to the smoothing kernel, this scheme is not able to capture discontinuities as well as other schemes mentioned before.\n",
    "    - 2nd-order MUSCL-Hancock scheme\n",
    "- AMR\n",
    "    - AMR can focus the computational power on regions of interest based on a set of criteria given by the simulator.\n",
    "    - cell-by-cell (tree-based) ([Khokhlov 1998](#ref-khokhlov-1998))\n",
    "    - patch-based ([Berger & Oliger 1984](#ref-berger-1984), [Berger & Collela 1989](#ref-berger-1989))\n",
    "        coplex data structure and data management\n",
    "    - tiling AMR (and their pros and cons)\n",
    "    \n",
    "In this paper, a new Radiation Hydrodynamics code based on Monte-Carlo Ray tracing solver together with an unsplit 2nd-order MUSCL-Hancock Hydro solver is presented."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Numerical Methods  <a class=\"anchor\" id=\"numerical-methods\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tile-Based Adaptive Mesh Refinement <a class=\"anchor\" id=\"num-amr\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hydrodynamical solver  <a class=\"anchor\" id=\"num-hyd\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Conservation laws  <a class=\"anchor\" id=\"num-hyd-con\"></a>\n",
    "\n",
    "Conservation laws of ideal compressible flows\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "\\frac{\\partial U}{\\partial t} + \\frac{\\partial f(U)}{\\partial x} + \\frac{\\partial g(U)}{\\partial x}  + \\frac{\\partial h(U)}{\\partial x} &= 0 \\\\\n",
    "Bu &= b \\\\\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "$$U = \\begin{pmatrix}\n",
    "\\rho \\\\\n",
    "\\rho u \\\\\n",
    "\\rho v \\\\\n",
    "\\rho w \\\\\n",
    "\\rho E \\\\\n",
    "\\end{pmatrix}\n",
    "\\quad\n",
    "f(U) = \\begin{pmatrix}\n",
    "\\rho u \\\\\n",
    "\\rho u^2 + p \\\\\n",
    "\\rho u v \\\\\n",
    "\\rho u w \\\\\n",
    "\\rho u E + u p \\\\\n",
    "\\end{pmatrix}\n",
    "g(U) = \\begin{pmatrix}\n",
    "\\rho v \\\\\n",
    "\\rho u v \\\\\n",
    "\\rho v^2 + p \\\\\n",
    "\\rho u w \\\\\n",
    "\\rho u E + u p \\\\\n",
    "\\end{pmatrix}\n",
    "h(U) = \\begin{pmatrix}\n",
    "\\rho v \\\\\n",
    "\\rho u w \\\\\n",
    "\\rho v w \\\\\n",
    "\\rho w^2 + p \\\\\n",
    "\\rho u E + u p \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "\n",
    "$$ p = (\\gamma - 1) (\\rho E - \\frac 1 2 \\rho (u^2 + v^2 + w^2))$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Slope Limiters  <a class=\"anchor\" id=\"num-hyd-slo\"></a>\n",
    "\n",
    "Suppose $U_i^n$ is the conserved variables at cell $i$ and time step $n$. During data reconstruction, we can extrapolate the cell centered values of the vector $U_i^n$ as follow,\n",
    "\n",
    "$$U_i(x) = U_i^n + \\frac{(x - x_i)}{\\Delta x} \\Delta_i$$\n",
    "\n",
    "where $\\Delta_i$ is the slope vector which can be find by calculating the difference of $U_i^n$ compare to its neighbors. The special case of the intercell boundaries $x_{i - \\frac 1 2}$ and $x_{i + \\frac 1 2}$ then can be easily reconstructed by extrapolating $U_i^n$,\n",
    "\n",
    "$$U_{i - \\frac 1 2} = U_i^n - \\frac 1 2 \\Delta_i, \\quad U_{i + \\frac 1 2} = U_i^n + \\frac 1 2 \\Delta_i$$\n",
    "\n",
    "Slope vector can be simply calculated as,\n",
    "\n",
    "$$\\Delta_i = \\frac 1 2 [(1 + \\omega) \\Delta_{i - \\frac 1 2} + (1 - \\omega) \\Delta_{i + \\frac 1 2}$$\n",
    "\n",
    "where,\n",
    "\n",
    "$$\\Delta_{i - \\frac 1 2} \\equiv U_i^n - U_{i-1}^n, \\quad \\Delta{i + \\frac 1 2} \\equiv U_{i + 1}^n - U_i^n$$\n",
    "\n",
    "and $\\omega \\in [-1, 1]$.\n",
    "\n",
    "$\\omega = 0$ will reduce the scheme to Fromm method and will produce spurious oscillations at strong gradients. Using Total Variation Diminishing (TVD) constraint [Harten 1983](#ref-harten-1983), we can resolve this problem.\n",
    "\n",
    "The TVD extension of the scheme can be achieved by replacing the slopes vector $\\Delta_i$ to limited slopes vector of $\\bar{\\Delta}_i$ defined as,\n",
    "\n",
    "$$\\bar{\\Delta}_i = \\xi_i \\Delta_i$$\n",
    "\n",
    "where with a proper choice of $\\xi_i$ we can satisfy the TVD constraint,\n",
    "\n",
    "$$\n",
    "\\begin{cases}\n",
    "\\xi(r) = 0 \\quad &\\text{for} \\quad r \\le 0\\\\\n",
    "0 \\le \\xi(r) \\le min\\{\\xi_L(r), \\xi_R(r)\\} \\quad &\\text{for} \\quad r > 0\\\\\n",
    "\\end{cases}\n",
    "$$\n",
    "\n",
    "where,\n",
    "\n",
    "$$\n",
    "\\begin{cases}\n",
    "\\xi_L(r) &= \\frac{2 \\beta_{i - \\frac 1 2} r}{1 - \\omega + (1 + \\omega) r} \\\\\n",
    "\\xi_R(r) &= \\frac{2 \\beta_{i + \\frac 1 2}}{1 - \\omega + (1 + \\omega) r} \\\\\n",
    "\\end{cases}\n",
    "$$\n",
    "\n",
    "and\n",
    "\n",
    "$$r = \\frac{\\Delta_{i - \\frac 1 2}}{\\Delta_{i + \\frac 1 2}}$$\n",
    "\n",
    "and\n",
    "\n",
    "$$\\beta_{i - \\frac 1 2} = \\frac{2}{1 + c}, \\beta_{i + \\frac 1 2} = \\frac{2}{1 - c}$$\n",
    "\n",
    "where $c$ is the Courant number."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### van Leer 1977 <a class=\"anchor\" id=\"num-hyd-slo-lee\"></a>\n",
    "\n",
    "$$\\xi_{vl}(r) = \\begin{cases}\n",
    "0, &r \\le 0, \\\\\n",
    "min\\{\\frac{2r}{1 + r}, \\xi_R(r)\\}, &r \\ge 0.\n",
    "\\end{cases}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def vanLeer(r, w):\n",
    "    def xi_R(r, w):\n",
    "       return 2 / (1 - w + (1 + w)*r)\n",
    "\n",
    "    return min(2 * r / (1 + r), xi_R(r, w))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tests  <a class=\"anchor\" id=\"tests\"></a>\n",
    "Make sure to also test Quirk’s and the blunt-body problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion  <a class=\"anchor\" id=\"conclusion\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Outlook  <a class=\"anchor\" id=\"outlook\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References  <a class=\"anchor\" id=\"references\"></a>\n",
    "\n",
    "* [Berger & Oliger 1984](https://www.mendeley.com/catalogue/d7df7cf0-7e19-3ce8-9a4d-8188e495786f/) Adaptive mesh refinement for hyperbolic partial differential equations <a class=\"anchor\" id=\"ref-berger-1984\"></a>\n",
    "* [Berger & Collela 1989](https://ui.adsabs.harvard.edu/abs/1989JCoPh..82...64B/abstract) Local Adaptive Mesh Refinement for Shock Hydrodynamics <a class=\"anchor\" id=\"ref-berger-1989\"></a>\n",
    "* [Cantalupo & Porciani 2011](https://ui.adsabs.harvard.edu/abs/2011MNRAS.411.1678C/abstract) RADAMESH: cosmological radiative transfer for Adaptive Mesh Refinement simulations <a class=\"anchor\" id=\"ref-cantalupo-2011\"></a>\n",
    "* [Harten 1983](https://www.sciencedirect.com/science/article/pii/0021999183901365?via%3Dihub) High resolution schemes for hyperbolic conservation laws <a class=\"anchor\" id=\"ref-harten-1983\"></a>\n",
    "* [Khokhlov 1998](https://ui.adsabs.harvard.edu/abs/1998JCoPh.143..519K/abstract) Fully Threaded Tree Algorithms for Adaptive Refinement Fluid Dynamics Simulations <a class=\"anchor\" id=\"ref-khokhlov-1998\"></a>\n",
    "* [Smith et al. 2020](https://arxiv.org/abs/2008.01750) Arepo-MCRT: Monte Carlo Radiation Hydrodynamics on a Moving Mesh <a class=\"anchor\" id=\"ref-smith-2020\"></a>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
